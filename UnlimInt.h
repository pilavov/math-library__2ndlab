//
// Created by kostya on 27/09/2019.
//

#include <vector>
#include <string>
#include <complex>

#ifndef INTMATHLIBRARY_UNLIMINT_H
#define INTMATHLIBRARY_UNLIMINT_H


class Multiplication_Algorithms;

class UnlimInt {
private:
    static Multiplication_Algorithms *p;
public:
    UnlimInt()= default;
    explicit UnlimInt(const char *str);
    explicit UnlimInt(std::string str);
    ~UnlimInt() = default;
    UnlimInt(const UnlimInt &other);
    UnlimInt(UnlimInt&& other) noexcept ;
    UnlimInt& operator=(const UnlimInt& other);
    UnlimInt& operator=(UnlimInt&& other)noexcept ;
    UnlimInt& operator=(const char *str);
    UnlimInt&operator=(const std::string &str);
    UnlimInt operator+(UnlimInt const &other)const;
    UnlimInt operator-(UnlimInt const &other)const;
    UnlimInt operator*(UnlimInt const &other)const;
    UnlimInt operator%(UnlimInt const &other)const;
    bool operator>=(UnlimInt const &other) const;
    friend std::ostream& operator<<(std::ostream& os,const UnlimInt& v) ;
    std::vector<char> getVector();
    static std::vector<char> findPower(const std::vector<char>& tmp);
    static UnlimInt constMult(const std::vector<char> &a,const int &n);
    static void deduct(UnlimInt const &a, UnlimInt const &other,UnlimInt &result);
    static void plus(UnlimInt const &a, UnlimInt const &b,UnlimInt &result,int rr);
    void shutter();
    void divideByN(int n);
    static UnlimInt fastModul(UnlimInt const &b,UnlimInt const &e,UnlimInt const &m);
    bool isPrime();

    std::vector<char> number;
};


class Multiplication_Algorithms: public UnlimInt {
public:
    static void setMult(int tmp = 1);
    static UnlimInt mult(UnlimInt const &a,UnlimInt const &b);
    static UnlimInt karasuba(UnlimInt const &a, UnlimInt const &b);
    static UnlimInt toomCook(UnlimInt const &a, UnlimInt const &b);
    static UnlimInt schonhageStrassen(UnlimInt const &a , UnlimInt const &b);
private:
    static void shutterBack(std::vector<int> & a);
    static UnlimInt plus1(std::vector<int> &a);
    static std::vector<int> multiplyPolinoms(std::vector<int> const& a, std::vector<int> const& b);
    using cd = std::complex<double>;
    static int currentMethodMult ;
    static void fft(std::vector<cd> & a, bool invert);
};

class Prime_Algorithms : public  UnlimInt{
private:
    static int currentMethodPrime ;
public:
    static void setPrime(int tmp = 0);
    static bool prime(UnlimInt const &a);
    static bool isPrimeFermat (UnlimInt const &obj);
    static bool isPrimeMillerRabin( UnlimInt const &obj);
    static bool isPrimeSolovayStrassen (UnlimInt const &obj);
};
#endif //INTMATHLIBRARY_UNLIMINT_H
