#include <iostream>
#include "UnlimInt.h"
#include <ctime>
#include <iomanip>

///    TIME RESEARCH   ///
/*
 *all researches have been made for 1000characters * 1000characters
 *
 * operator+ 0.000128 seconds
 *
 * operator- 0.000127 seconds
 *
 *1.85 seconds <---- this is Karasuba method
 *
 * Toom-Cook 1 second
 *
 * schonhageStrassen 0.008 second
 */

int Multiplication_Algorithms::currentMethodMult = 1;
int Prime_Algorithms::currentMethodPrime = 0;

int main() {
    std::string thousn ;
    for (int i = 0; i < 1000; ++i) {
        thousn += '8';
    }
    std::string eigth ;
    for (int i = 0; i < 1000; ++i) {
        eigth += '9';
    }
    UnlimInt nine(thousn),eight(eigth);
    UnlimInt a("1234567890123456789012"),b("9876543210987654321098"), c("1217"),dva("12345678912"),odin("21987543215");
    Multiplication_Algorithms::setMult(2);
    Prime_Algorithms::setPrime(0);
    unsigned int start =clock();
    std::cout<<nine*eight<<std::endl;
    unsigned int end = clock();
    std::cout<<std::fixed<<double((end - start))/CLOCKS_PER_SEC<<std::endl;
    return 0;
}
