//
// Created by kostya on 27/09/2019.
//
#include <iostream>
#include "UnlimInt.h"


// have to add check for a string to be a number only and check for emptiness
UnlimInt::UnlimInt(const char *str) {
    int i = 0;
    do{
        number.push_back(*(str+i));
        i++;
    }while(*(str+i) != '\0');
}

UnlimInt::UnlimInt(const UnlimInt &other) {
    number=other.number;
}

UnlimInt::UnlimInt(UnlimInt &&other) noexcept {
    number = other.number;
    other.number.clear();
}

// have to add check for a string to be a number only and check for emptiness
UnlimInt& UnlimInt::operator=(const char * str) {
    number.clear();
    int i = 0;
    do{
        number.push_back(*(str+i));
        i++;
    }while(*(str+i) != '\0');
    return *this;
}

UnlimInt &UnlimInt::operator=(const UnlimInt &other) {
    if (&other != this) {
        number=other.number;
    }
    return *this;
}

UnlimInt &UnlimInt::operator=(UnlimInt &&other) noexcept {
    if (&other != this) {
        number=other.number;
        other.number.clear();
    }
    return *this;
}

std::ostream &operator<<(std::ostream &os, const UnlimInt &v) {
    for (auto const &i: v.number) {
        os << i ;
    }
    return os;
}

UnlimInt UnlimInt::operator-(UnlimInt const &other) const{
    UnlimInt diff;
    if(number.size() == 1 && number[0]=='0'){
        diff = other;
        return diff;
    } else if(other.number.size() == 1 && other.number[0]=='0'){
        diff = *this;
        return diff;
    }
    if(number == other.number) {
        diff.number.push_back('0');
    }
    else if(*this>=other){
        deduct(*this,other, diff);
    }
    else{
        deduct(other,*this,diff);
    }
    return diff;
}

bool UnlimInt::operator>=(UnlimInt const &other)const {
    if(number.size()!=other.number.size()){
        return number.size()>=other.number.size();
    }
    else{
        for (int i = 0; i < number.size(); ++i) {
            if (number[i] != other.number[i])
                return number[i]> other.number[i];
        }
    }
    return true;
}

void UnlimInt::deduct(UnlimInt const &a,UnlimInt const &b,UnlimInt &result) {
    int aValue, aValueNext = int(a.number[a.number.size() -1]) -48, bValue;
    int i = 0;
    for (; i < b.number.size() ; ++i) {
        aValue = aValueNext;
        aValueNext = int(a.number[a.number.size() - i - 2]) - 48;
        bValue = int(b.number[b.number.size() - i -1 ]) - 48;
        aValue -= bValue;
        if (aValue < 0) {
            aValue += 10;
            aValueNext--;
        }
        result.number.insert(result.number.begin(),char(aValue + 48));
    }
    if(a.number.size()!=b.number.size()){
        for(;i<a.number.size() - 1;i++){
            aValue = int(a.number[a.number.size() -i - 2]) -48;
            if(aValueNext<0){
                aValue--;
                aValueNext+=10;
            }
            result.number.insert(result.number.begin(), char(aValueNext + 48));
            aValueNext = aValue;
        }
        result.number.insert(result.number.begin(), char(aValueNext + 48));
    }
    result.shutter();
}

UnlimInt UnlimInt::operator+(UnlimInt const &other) const {
    UnlimInt sum;
    int pos = 0,length = 0, r = 0;
    int aValue, bValue, resValue;
    if(*this>=other){
        length = other.number.size();
    }
    else{
        length = number.size();
    }
    for (int i = 0; i < length; ++i) {
        aValue = int(number[number.size() - i -1]) - 48;
        bValue = int(other.number[other.number.size() - i - 1]) -48;
        resValue = aValue + bValue + r;
        if(resValue >= 10){
            resValue -= 10;
            r = 1;
        } else{
            r = 0;
        }
        sum.number.insert(sum.number.begin(), char(resValue+48));
    }
    if (number.size()  == other.number.size() && r == 1){
        sum.number.insert(sum.number.begin(), '1');
    }
    else if(number.size() > other.number.size()){
        plus(*this,other,sum,r);
    }
    else{
        plus(other,*this,sum,r);
    }
    return sum;
}

///a>b
void UnlimInt::plus(UnlimInt const &a, UnlimInt const &b, UnlimInt &result,int rr) {
    int aValue = 0;
    for (int i = b.number.size() ; i < a.number.size(); ++i) {
        aValue = int(a.number[a.number.size() - i -1]) - 48;
        aValue += rr;
        if(aValue >= 10){
            aValue -= 10;
            rr = 1;
        } else{
            rr = 0;
        }
        result.number.insert(result.number.begin(), char(aValue+48));
    }
    if(rr == 1){
        result.number.insert(result.number.begin(), '1');
    }
}

UnlimInt::UnlimInt(std::string str) {
    int length = str.length();
    for (int i = 0; i < length; ++i) {
        number.push_back(str[i]);
    }
}

std::vector<char> UnlimInt::findPower(const std::vector<char>& tmp) {
    int power = tmp.size() ;
    std::vector<char> m;
    for (int i = 0; i < power - 1; ++i) {
        m.push_back('0');
    }
    m.insert(m.begin(), '1');
    return m;
}

UnlimInt UnlimInt::operator*(UnlimInt const &other) const{
    return Multiplication_Algorithms::mult(*this,other);
}

UnlimInt UnlimInt::constMult(const std::vector<char> &a, const int &n) {
    UnlimInt res;
    int r = 0;
    int result;
    if(n == 0){
        res.number.push_back('0');
        return res;
    }
    for (int i=0;i<a.size() ;i++)
    {
        result = int(a[a.size() - i -1] - 48) * n + r;
        r = result/10;
        result -= r*10;
        res.number.insert(res.number.begin(),char(result + 48));
    }
    if(r!= 0 ){
        res.number.insert(res.number.begin(),char(r +48));
    }
    return res;
}

void UnlimInt::shutter() {
    int j = 0;
    for (; this->number[j] == '0' && j <this->number.size() -1  ; j++) {}
    this->number.erase(this->number.begin(),this->number.begin() + j);
}


void UnlimInt::divideByN(int n) {
    UnlimInt odin("1");
    int ost = 0;
    int result;
    int tmp;
    for (int i = 0; i < this->number.size();i++) {
        tmp = int(this->number[i] - 48) + ost*10;
        result = tmp / n;
        ost = tmp % n;
        this->number[i]  = char(result + 48);
    }
    if(ost == 1){
        *this = *this + odin;
    }
    this->shutter();
}



std::vector<char> UnlimInt::getVector() {
    return this->number;
}

UnlimInt &UnlimInt::operator=(const std::string &str) {
    // have to add check for a string to be a number only and check for emptiness
        number.clear();
        int i = 0;
        do{
            number.push_back(str[i]);
            i++;
        }while(str[i] != '\0');
        return *this;
}


UnlimInt UnlimInt::operator%(UnlimInt const &other) const {
    UnlimInt result;
    std::string str = "0";
    if(this->number == other.number){
        result=str;
        return result;
    } else if (other>=*this){
        return *this;
    }
    else {
        result = *this;
        while (result >= other) {
            result = result - other;
        }
    }
    return result;
}

UnlimInt UnlimInt::fastModul(UnlimInt const &b, UnlimInt const &e, UnlimInt const &m)  {
    UnlimInt one("1"),eShtrih("1"),a("1");
    Multiplication_Algorithms::setMult(1);
    while (eShtrih.number != e.number){
        a = (a*b)%m;
        eShtrih = eShtrih + one;
    }
    a = (a*b)%m;
    return a;
}

bool UnlimInt::isPrime() {
    return Prime_Algorithms::prime(*this);
}




void Multiplication_Algorithms::setMult(int tmp) {

        Multiplication_Algorithms::currentMethodMult = tmp ;

}

UnlimInt Multiplication_Algorithms::mult(UnlimInt const &a,UnlimInt const &b) {
    UnlimInt result;
    if(Multiplication_Algorithms::currentMethodMult == 0){
        result = karasuba(a,b);
    } else if (Multiplication_Algorithms::currentMethodMult == 1){
        result = toomCook(a,b);
    } else{
        result = schonhageStrassen(a,b);
    }
    return result;
}

UnlimInt Multiplication_Algorithms::toomCook(UnlimInt const &a, UnlimInt const &b) {
    if(a.number.size()<9 || b.number.size() < 9){
        return karasuba(a,b);
    }

    UnlimInt result;
    UnlimInt m2,m1,m0,n2,n1,n0;
    UnlimInt dodanok1,dodanok2,dodanok3;
    UnlimInt w4,w3,w2,w1,w0;

    int zeroQuantity;
    int sizeA =a.number.size();
    int sizeB =b.number.size();
    int pos = (sizeA%3)*(sizeA - 2*(sizeA/3 + sizeA%3));
    if(pos == 0){
        pos = sizeA /3;
    }
    int secPos = (sizeA - pos)/2;
    if(abs(sizeA - sizeB)>secPos){
        result = karasuba(a,b);
        return result;
    }
    m2.number.assign(a.number.begin(),a.number.begin() + pos);

    m1.number.assign(a.number.begin() + pos, a.number.begin() + pos + secPos);
    m0.number.assign(a.number.begin() + pos + secPos,a.number.end());


    pos = (sizeB%3)*(sizeB - 2*(sizeB/3 + sizeB%3));
    if(pos == 0){
        pos = sizeB /3;
    }
    n2.number.assign(b.number.begin(),b.number.begin() + pos);
    secPos = (sizeB - pos)/2;
    n1.number.assign(b.number.begin() + pos, b.number.begin() + pos + secPos);
    n0.number.assign(b.number.begin() + pos + secPos,b.number.end());

    zeroQuantity = n0.number.size();

    m2.shutter();
    m1.shutter();
    m0.shutter();
    n2.shutter();
    n1.shutter();
    n0.shutter();

    dodanok1 = karasuba((m2+m1+m0),(n2+n1+n0));
    dodanok2 = karasuba((m2+m0-m1),(n2+n0-n1));
    dodanok3 = karasuba((constMult(m2.number,4)+m0-constMult(m1.number,2)),(constMult(n2.number,4)+n0-constMult(n1.number,2)));

    w0 = karasuba(m0 , n0);
    w4 = karasuba(m2 , n2);

    w2 = dodanok1 + dodanok2 - constMult(w0.number,2) - constMult(w4.number,2);
    w2.divideByN(2);

    w3 = constMult(constMult(w4.number,2).number,7) + constMult(w2.number,2) + constMult(dodanok2.number,2) - w0 - dodanok3;
    w3.divideByN(6);

    w1  = w4 + w2 + w0 - w3 - dodanok2;

    for (int i = 0; i < zeroQuantity; ++i) {
        w1.number.push_back('0');
        w2.number.push_back('0');
        w3.number.push_back('0');
        w4.number.push_back('0');
    }
    for (int j = 0; j < zeroQuantity; ++j) {
        w2.number.push_back('0');
        w3.number.push_back('0');
        w4.number.push_back('0');
    }
    for (int k = 0; k < zeroQuantity; ++k) {
        w3.number.push_back('0');
        w4.number.push_back('0');
    }
    for (int l = 0; l < zeroQuantity; ++l) {
        w4.number.push_back('0');
    }
    result = w4 + w3 + w2 + w1 + w0;
    return result;
}

UnlimInt Multiplication_Algorithms::karasuba(UnlimInt const &a, UnlimInt const &b) {
    UnlimInt z0,z1,z2;
    UnlimInt highFromFirst;
    UnlimInt highFromSecond;
    UnlimInt high1FromFirst;
    UnlimInt high1FromSecond;
    UnlimInt lowFromFirst;
    UnlimInt lowFromSecond;
    UnlimInt result;
    std::vector<char> m;
    if(a.number.size() == 1 || b.number.size() == 1){
        if(a.number.size() ==1){
            return constMult(b.number,int(a.number[0] - 48));
        } else{
            return constMult(a.number,int(b.number[0] - 48));
        }
    }
    if(a.number.size() == 1 && a.number[0] == 1 ){
        result = b;
        return result;
    }
    if(b.number.size() == 1 && b.number[0] == 1){
        result = a;
        return result;
    }
    if(a>=b){
        m = findPower(b.number);
        lowFromFirst.number.assign(b.number.begin()+(b.number.size() - (m.size() -1)),b.number.end());
        lowFromSecond.number.assign(a.number.begin()+(a.number.size() - (m.size()-1)), a.number.end());
        highFromFirst.number.insert(highFromFirst.number.begin(), b.number[0]);
        int length = a.number.size() - b.number.size();
        for (int i = 0; i <= length; ++i) {
            highFromSecond.number.push_back(a.number[i]);
        }
    }
    else{
        m = findPower(a.number);
        lowFromFirst.number.assign(a.number.begin()+(a.number.size() - (m.size()-1)), a.number.end());
        lowFromSecond.number.assign(b.number.begin()+(b.number.size() - (m.size() -1)),b.number.end());
        highFromFirst.number.insert(highFromFirst.number.begin(), a.number[0]);
        int length =  b.number.size() - a.number.size();
        for (int i = 0; i <= length; ++i) {
            highFromSecond.number.push_back(b.number[i]);
        }
    }
    lowFromFirst.shutter();
    lowFromSecond.shutter();
    high1FromFirst = highFromFirst;
    high1FromSecond = highFromSecond;
    for (int i = 0; i < m.size() - 1; ++i) {
        high1FromFirst.number.push_back('0');
        high1FromSecond.number.push_back('0');
    }
    z0 = karasuba(lowFromFirst , lowFromSecond);
    lowFromFirst = lowFromFirst + highFromFirst;
    lowFromSecond = lowFromSecond + highFromSecond;
    z1 = karasuba(lowFromFirst , lowFromSecond);
    z2 = karasuba(highFromFirst , highFromSecond);
    UnlimInt tmp = z2;
    UnlimInt tmp1 = z1 - z2 - z0;
    for (int k = 0; k < 2*(m.size() - 1); ++k) {
        tmp.number.push_back('0');
    }
    for (int p = 0; p < m.size() -1; ++p) {
        tmp1.number.push_back('0');
    }
    result = tmp + tmp1 + z0;
    return result;
}

UnlimInt Multiplication_Algorithms::schonhageStrassen(UnlimInt const &a, UnlimInt const &b) {
    UnlimInt result;
    std::vector<int> vec1;
    std::vector<int> vec2;
    std::vector<int> vecRes;
    vec1.reserve(a.number.size());
    vec2.reserve(b.number.size());
    for (char i : a.number) {
            vec1.push_back(i-48);
        }
    for (char i : b.number) {
        vec2.push_back(i-48);
    }
    vecRes = multiplyPolinoms(vec1,vec2);
    shutterBack(vecRes);
    result = plus1(vecRes);
    return result;
}

void Multiplication_Algorithms::fft(std::vector<cd> &a, bool invert) {
    const double PI = acos(-1);
    int n = a.size();
    if (n == 1)
        return;

    std::vector<cd> a0(n / 2), a1(n / 2);
    for (int i = 0; 2 * i < n; i++) {
        a0[i] = a[2*i];
        a1[i] = a[2*i+1];
    }
    fft(a0, invert);
    fft(a1, invert);

    double ang = 2 * PI / n * (invert ? -1 : 1);
    cd w(1), wn(cos(ang), sin(ang));
    for (int i = 0; 2 * i < n; i++) {
        a[i] = a0[i] + w * a1[i];
        a[i + n/2] = a0[i] - w * a1[i];
        if (invert) {
            a[i] /= 2;
            a[i + n/2] /= 2;
        }
        w *= wn;
    }
}

std::vector<int> Multiplication_Algorithms::multiplyPolinoms(std::vector<int> const& a, std::vector<int> const& b) {
    std::vector<cd> fa(a.begin(), a.end()), fb(b.begin(), b.end());
    int n = 1;
    while (n < a.size() + b.size())
        n <<= 1;
    fa.resize(n);
    fb.resize(n);

    fft(fa, false);
    fft(fb, false);
    for (int i = 0; i < n; i++)
        fa[i] *= fb[i];
    fft(fa, true);

    std::vector<int> result(n);
    for (int i = 0; i < n; i++)
        result[i] = round(fa[i].real());
    return result;
}

UnlimInt Multiplication_Algorithms::plus1(std::vector<int> &a) {
    UnlimInt result;

    int digit = 0 , cur = 0;
    int vecSize = a.size() - 1;

    while(vecSize >= 0) {
        a[vecSize] += digit;
        if (a[vecSize] != 0) {
            digit = a[vecSize] / 10;
            cur = a[vecSize] % 10;
            if (cur > 9) {
                cur = cur % 10;
            }
            result.number.insert(result.number.begin(),cur +48);
        }
        vecSize--;
    }
    if(digit != 0){
        result.number.insert(result.number.begin(),digit+48);
    }
    return result;
}

void Multiplication_Algorithms::shutterBack(std::vector<int> &a) {
        int j = a.size() - 1;
        for (; a[j] == 0 && j > 0  ; j--) {}
        a.erase(a.begin() + j + 1,a.end());
}

void Prime_Algorithms::setPrime(int tmp) {
    Prime_Algorithms::currentMethodPrime = tmp ;
}

bool Prime_Algorithms::prime(UnlimInt const &a) {
    bool flag ;
    if(Prime_Algorithms::currentMethodPrime == 0){
        flag = isPrimeFermat(a);
    } else if (Prime_Algorithms::currentMethodPrime == 1){
        flag = isPrimeMillerRabin(a);
    } else{
        flag = isPrimeSolovayStrassen(a);
    }
    return flag;
}

bool Prime_Algorithms::isPrimeFermat(UnlimInt const &obj) {
    UnlimInt a("1"),one("1"),tenMil("999999"),tmp = obj - one;
    UnlimInt tmpForPower;
    bool flag = true;
    srand(time(nullptr));
    unsigned int k= random() % 1 + 1;
    unsigned int base[3] = {2,3};
    unsigned int currentBase;
    if (obj.number.size() == 1 && (obj.number[0] == '1' || obj.number[0] == '3')){
        return true;
    }
    if((int(obj.number[obj.number.size() - 1]) - 48 ) % 2 == 0){
        return false;
    }

    while(k != 0){
        currentBase = base[k-1] ;
        tmp = obj - one;
        a = one;
        for (unsigned int i = 0; i <  currentBase - 1; ++i) {
            a = a + one ;
        }
        a = UnlimInt::fastModul(a,tmp,obj);
        if (a.number != one.number){
            flag = false;
        }
        k--;
    }
    return flag;
}

bool Prime_Algorithms::isPrimeMillerRabin(UnlimInt const &obj) {
    if((int(obj.number[obj.number.size() - 1]) - 48 ) % 2 == 0){
        return false;
    }
    UnlimInt x , a,tmp;
    bool flag = false;
    std::vector<std::string> strings = { "2","3", "5", "7", "11", "13", "17", "19","23", "29", "31", "37", "41"};
    UnlimInt twoPow("1"),two ("2"), pow("1") , zero("0") , one("1"), d("1"),current;
    srand(time(nullptr));
    unsigned int k= random() % 12 + 2;
    while(k!=0) {
        unsigned int n= random() % 12 ;
        a = strings[n];
        while (obj - one >= two) {
            if (fastModul(obj - one, pow, two).number == zero.number) {
                current = two;
            }
            two = two + two;
        }
        two = "2";
        while ((current * d).number != (obj - one).number) {
            d = d + two;
        }
        for (; two.number != current.number; twoPow = twoPow + one) {
            current.divideByN(2);
        }
        x = fastModul(a, d, obj);
        if (x.number == one.number || x.number == (obj - one).number) {
            k--;
            continue;
        }
        while ((twoPow - one).number != zero.number){
            x = fastModul(x,two,obj);
            if( x.number == (obj - one).number){
                k--;
                flag = true;
                break;
            }
            twoPow = twoPow - one;
        }
        if (flag){
            continue;
        }
        return false;
    }
    return true;
}

bool Prime_Algorithms::isPrimeSolovayStrassen(UnlimInt const &obj){
    if((int(obj.number[obj.number.size() - 1]) - 48 ) % 2 == 0){
        return false;
    }
    srand(time(nullptr));
    unsigned int k= random() % 12 + 2;
    int n = obj.number.size() ;
    std::string str;
    UnlimInt a,one("1");
    UnlimInt pow = obj - one;
    pow.divideByN(2);
    while(k!=0) {
        str = "";
        int var = random()% (n-1);
        for (int i = 0; i <= var ; ++i) {
            str += random()%9 +49;
        }
        a =str;
        if((fastModul(a, pow, obj).number == (obj - one).number) || (fastModul(a, pow, obj).number == one.number)){
            k--;
            continue;
        }
        return false;
    }
    return true;
}
