cmake_minimum_required(VERSION 3.10)
project(IntMathLibrary)

set(CMAKE_CXX_STANDARD 17)

add_executable(IntMathLibrary main.cpp UnlimInt.cpp UnlimInt.h)
add_executable(Test test/test.cpp )

target_sources(Test PRIVATE
        ../googletest/googletest/src/gtest-all.cc
        )

target_include_directories(Test PRIVATE
        ../googletest/googletest
        ../googletest/googletest/include
        )

target_link_libraries(Test
        pthread
        )
